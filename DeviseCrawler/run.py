import scrapy
from scrapy.crawler import CrawlerProcess
from DeviseCrawler.spiders.dinartunisien import DinartunisienSpider

process = CrawlerProcess(settings={
    "FEEDS": {
        "items.json": {"format": "json"},
    },
})

process.crawl(DinartunisienSpider)
process.start() 