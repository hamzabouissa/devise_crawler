# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DevisecrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    code = scrapy.Field()
    unite = scrapy.Field()
    achat = scrapy.Field()
    vente = scrapy.Field()
    bank = scrapy.Field()
