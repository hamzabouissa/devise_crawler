import scrapy
from bs4 import BeautifulSoup
from DeviseCrawler.items import DevisecrawlerItem


class AttijaribankSpider(scrapy.Spider):
    name = 'attijaribank'
    allowed_domains = ['attijaribank.com.tn']
    start_urls = ['http://www.attijaribank.com.tn/Fr/Cours_de_change__59_205']

    def parse(self, response):

        page = BeautifulSoup(response.text, "lxml")
        center_page = page.find("div",class_="center_page")
        currency_table = center_page.div.table
        devise = currency_table.find_all("td")
        result = []
        for idx in range(0, len(devise), 7):
            result.append(DevisecrawlerItem(
                code="".join(devise[idx + 1].stripped_strings),
                unite="".join(devise[idx + 2].stripped_strings),
                achat="".join(devise[idx + 3].stripped_strings),
                vente="".join(devise[idx + 4].stripped_strings),
                bank="bna"
            ))
        return result
