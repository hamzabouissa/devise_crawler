import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup
from DeviseCrawler.items import DevisecrawlerItem
import re

class BnaSpider(CrawlSpider):
    name = 'bna'
    allowed_domains = ['www.bna.tn']
    start_urls = ['http://www.bna.tn/site/fr/devise.php?id_article=188']

    # rules = (
    #     Rule(LinkExtractor(allow=r'Items/'), callback='parse_item', follow=True),
    # )

    def parse_start_url(self, response):
        page = BeautifulSoup(response.text,"lxml")
        currency_table = page.find("table", class_='currency-table')
        devise = currency_table.find_all("td", class_=re.compile("ligne_devise_interne"))
        result = []
        for idx in range(0,len(devise),4):
            result.append(DevisecrawlerItem(
                code="".join(devise[idx].stripped_strings),
                unite="".join(devise[idx+1].stripped_strings),
                achat="".join(devise[idx+2].stripped_strings),
                vente="".join(devise[idx+3].stripped_strings),
                bank="bna"
            ))

        return result
