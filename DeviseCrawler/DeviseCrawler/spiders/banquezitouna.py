import re
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup
from DeviseCrawler.items import DevisecrawlerItem

class BanquezitounaSpider(CrawlSpider):
    name = 'banquezitouna'
    allowed_domains = ['www.banquezitouna.com']
    start_urls = [
        'http://www.banquezitouna.com/Fr/tableau-de-change_64_103',
    ]

    # rules = (
    #     Rule(LinkExtractor(allow=r'Items/'), callback='parse_item', follow=True),
    # )

    def parse_start_url(self, response, **kwargs):
        page = BeautifulSoup(response.text,"lxml")
        currency_table = page.find("div", class_="b-exchange-table")
        # date_tag = page.find("span", class_="exchange-date").string
        # pattern = "(0[1-9]|[12]\d|3[01])-(0[1-9]|1[0-2])-[12]\d{3}"
        # creation_date = re.search(pattern, date_tag)
        # print(creation_date.group(0))
        devise = currency_table.find_all("div", class_="td")
        result = []
        for idx in range(6,len(devise)-6,6):
            result.append(DevisecrawlerItem(
                code="".join(devise[idx+1].stripped_strings),
                unite="".join(devise[idx+2].stripped_strings),
                achat="".join(devise[idx+3].stripped_strings),
                vente="".join(devise[idx+4].stripped_strings),
                bank="zitouna"
            ))
        return result
