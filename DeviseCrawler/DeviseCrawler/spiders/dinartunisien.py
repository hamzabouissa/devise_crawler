import scrapy
from bs4 import BeautifulSoup
from DeviseCrawler.items import DevisecrawlerItem
import re

class DinartunisienSpider(scrapy.Spider):
    name = 'dinartunisien'
    allowed_domains = ['dinartunisien.com']
    start_urls = [
        # 'https://www.dinartunisien.com/fr/cours-devises-biat',
        # 'https://www.dinartunisien.com/fr/cours-devises-atb',
        # 'https://www.dinartunisien.com/fr/cours-devises-attijari-bank',
        'https://www.dinartunisien.com/fr/cours-devises-banques-tunisie'
    ]

    def get_devise_info(self, response, **kwargs):
        bank = response.url.split("cours-devises-")[1]
        page = BeautifulSoup(response.text, "lxml")
        currency_table = page.find("table", id="rates-tables")
        # date_tag = page.find("div", class_="caption_tabchange").string
        # pattern = "(0[1-9]|[12]\d|3[01])-(0[1-9]|1[0-2])-[12]\d{3}"
        # creation_date = re.search(pattern, date_tag)


        devise = currency_table.find_all("td")
        result = []
        if bank == "banque-centrale-de-tunisie":
            for idx in range(0, len(devise), 6):
                result.append(DevisecrawlerItem(
                    code="".join(devise[idx+1].stripped_strings),
                    unite="".join(devise[idx + 2].stripped_strings),
                    vente="".join(devise[idx + 3].stripped_strings),
                    achat=0,
                    bank=bank
                ))
        else:
            for idx in range(0, len(devise), 7):
                result.append(DevisecrawlerItem(
                    code="".join(devise[idx+1].stripped_strings),
                    unite="".join(devise[idx + 2].stripped_strings),
                    achat="".join(devise[idx + 3].stripped_strings),
                    vente="".join(devise[idx + 4].stripped_strings),
                    bank=bank
                ))
        return result

    def parse(self, response, **kwargs):
        page = BeautifulSoup(response.text, "lxml")
        banks = page.find_all("div", class_="col-6 col-sm-4 col-lg-4 mb-lg-0 mb-5")
        for bank in banks[:-1]:
            link = bank.a.attrs['href']
            yield scrapy.Request(link, self.get_devise_info)
    
   
