import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup
import re
from DeviseCrawler.items import DevisecrawlerItem

class BhSpider(CrawlSpider):
    name = 'bh'
    allowed_domains = ['bh.com.tn']
    # start_urls = ['http://www.bh.com.tn/les-cours-de-change']

    def start_requests(self):
        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.5",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": 1,
            "If-None-Match": "1597679444-0",
            "Cache-Control": "max-age=0"
        }
        yield scrapy.Request("http://www.bh.com.tn/les-cours-de-change", headers=headers)

    # rules = (
    #     Rule(LinkExtractor(allow=r'Items/'), callback='parse_item', follow=True),
    # )

    def parse_start_url(self, response, **kwargs):
        page = BeautifulSoup(response.text,"lxml")
        devise = page.find_all("td",class_=re.compile("^views-field views-field-"))
        result = []
        for idx in range(0,len(devise),7):
            result.append(DevisecrawlerItem(
                code="".join(devise[idx+1].stripped_strings),
                unite="".join(devise[idx+3].stripped_strings),
                achat="".join(devise[idx+4].stripped_strings),
                vente="".join(devise[idx+5].stripped_strings),
                bank="bh"
            ))
        return result