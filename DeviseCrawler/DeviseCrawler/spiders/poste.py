import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from bs4 import BeautifulSoup

from DeviseCrawler.items import DevisecrawlerItem


class PosteSpider(CrawlSpider):
    name = 'poste'
    allowed_domains = ['poste.tn']
    start_urls = ['http://www.poste.tn/change.php']

    def parse_start_url(self, response, **kwargs):
        page = BeautifulSoup(response.text, "lxml")
        devise = page.find_all("td", class_="cel_contenu")
        result = []
        for idx in range(0, len(devise), 5):
            result.append(DevisecrawlerItem(
                code="".join(devise[idx+1].stripped_strings),
                unite="".join(devise[idx + 2].stripped_strings),
                achat="".join(devise[idx + 3].stripped_strings),
                vente="".join(devise[idx + 4].stripped_strings),
                bank="poste"
            ))
        return result
